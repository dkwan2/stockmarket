import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Start the RMI Server application that monitors and returns stock market updates.
 * @author Darlene Kwan, Josh O'Connor
 */
public class StartServer {
	
	/**
	 * Execution point of server application.
	 * @param args Accepts any arguments passed to program.
	 */
	public static void main(String args[]) {
		
		try {
			Registry registry = LocateRegistry.createRegistry(1099);
			registry.bind("Server", new RMIServer());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}