import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import javax.swing.JApplet;

/**
 * Start the RMI Client application that retrieves and displays stock updates from server.
 * @author Darlene Kwan, Josh O'Connor
 */
public class RMIClient extends JApplet implements Runnable {

	private static final int width = 400;
	private static final int height = 100;

	private Registry registry;
	private RMIServerInterface server;
	private Thread thread;
	private ArrayList<String> stocks;
	private String current = "";
	private int x = width / 4;
	private int y = 0;

	/**
	 * Initializes the applet and starts the client thread.
	 */
	public void init() {
		setSize(width, height);
		setBackground(Color.BLACK);

		try {
			registry = LocateRegistry.getRegistry(1099);
			server = (RMIServerInterface) registry.lookup("Server");
		} catch (Exception e) {
			e.printStackTrace();
		}

		thread = new Thread(this);
		thread.start();
	}

	/**
	 * Client thread that retrieves stock values from server. 
	 */
	@Override
	public void run() {
		while (true) {
			try {
				stocks = server.getStocks();
			} catch (RemoteException e) {
				e.printStackTrace();
			}

			int counter = 0;
			while (counter < stocks.size()) {
				current = stocks.get(counter);
				if (y <= height) {
					y += 1;
				} else {
					y = 0;
					counter++;					
				}
				if (y == (height / 2)) {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				repaint();
			}
		}
	}

	/**
	 * Renders and displays stock values in applet. 
	 * @param g The Graphics object used for drawing
	 */
	public void paint(Graphics g) {
		g.clearRect(0, 0, width, height);
		g.setColor(Color.CYAN);
		g.setFont(new Font("Arial", Font.BOLD, 24));
		g.drawString(current, x, y);
	}
}