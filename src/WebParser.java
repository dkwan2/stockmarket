import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
/**
 * Description: This class parses data off the web page.
 * @author Josh
 *
 */
public class WebParser {
private static final String URL = "http://tmx.quotemedia.com/marketsca.php?locale=EN";
private static final String PAGESECTION = "pageLeft";
private static final String PAGECURRENT = "last";
private static final String PAGEINCREASE = "qm-quote-data";
private Element tsxElement;

/**
 * Description: Grabs the tsx element that contains the data
 * @throws Exception
 */
public void Update () throws Exception {
	try{
		Document doc = Jsoup.connect(URL).get();
		Elements elements = doc.getElementsByClass(PAGESECTION);
		Element element = elements.get(0);
		Elements tsxElements = element.getElementsByAttributeValue("style", "width: 206px; float: left;");
		tsxElement = tsxElements.get(0);
	}
	catch(Exception e)
	{
		throw new Exception("Failed to pull page data.");
		//ToDo Handle Exception
	}
}
/**
 * Description: Grabs the current data from the tsx element.
 * @return String
 */
public String getCurrent (){
	String value = "";
	try{
		if(!(tsxElement.outerHtml() == "")){
			Elements temp = tsxElement.getElementsByClass(PAGECURRENT);
			Element temp2 = temp.get(0);
			Node span  = temp2.childNode(1);
			value = span.outerHtml().replace("<span>", "").replace("</span>", "");
		}
	}catch(Exception e)
	{
		throw new Exception("Failed to get current data.");
		//ToDo Handle Exception
	}
	finally{
		return value;
	}
}
/**
 * Description: Grabs the current data from the tsx element
 * @return String
 */
public String getIncrease (){
	String value = "";
	try{
		if(!(tsxElement.outerHtml() == "")){
			Elements temp = tsxElement.getElementsByClass(PAGEINCREASE);
			Element temp2 = temp.get(0);
			Node span  = temp2.childNode(3).childNode(1);
			value = span.outerHtml().replace("<span>", "").replace("</span>", "");
		}
	}catch(Exception e)
	{
		throw new Exception("Failed to get Increase data");
		//ToDo handle Exception
	}
	finally{
		return value;
	}
}
}
