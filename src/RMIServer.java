import java.rmi.*;
import java.rmi.server.*;
import java.util.ArrayList;

/**
 * Server application retrieves and returns stock updates.
 * @author Darlene Kwan, Josh O'Connor
 */
public class RMIServer extends UnicastRemoteObject implements RMIServerInterface {
	
	private String current;
	private String increase;

	/**
	 * Uses a ServerMonitor to watch stocks for data change events and then re-retrieves stock values.
	 * @throws RemoteException
	 */
	protected RMIServer() throws RemoteException {
		super();
		final ServerMonitor monitor = new ServerMonitor();
		monitor.addMyEventListener(new DataChangeEvent() {
			@Override
			public void DataChangedOccurred(MyEvent evt) {
				current = monitor.getCurrent();
				increase = monitor.getIncrease();
			}			
		});
	}

	/**
	 * Sends stock values as an String ArrayList.
	 * @return Stock values
	 */
	public ArrayList<String> getStocks() throws RemoteException {
		ArrayList<String> stocks = new ArrayList<String>();
		stocks.add(current);
		stocks.add(increase);
		return stocks;
	}
}