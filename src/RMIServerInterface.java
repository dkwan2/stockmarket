import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Server interface used by client application.
 * @author Darlene Kwan, Josh O'Connor
 */
public interface RMIServerInterface extends Remote {
	
	/**
	 * Defines the getStocks() method signature.
	 * @return Stock values
	 * @throws RemoteException
	 */
	public ArrayList<String> getStocks() throws RemoteException;
	
}