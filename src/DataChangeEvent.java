import java.util.EventListener;
/**
 * Description: this event listener is used to handle updates from server monitor change. 
 * @author Josh
 *
 */
public interface DataChangeEvent extends EventListener{
	/**
	 * Discription: this method will fire when data change is detected.
	 * @param evt
	 */
	public void DataChangedOccurred(MyEvent evt);
}
