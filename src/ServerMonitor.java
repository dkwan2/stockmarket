import java.util.EventObject;
import javax.swing.event.EventListenerList;
/**
 * Description: this class extends thread and checks for updates frequently.
 * @author Josh
 *
 */
public class ServerMonitor extends Thread{
	
	  protected EventListenerList listenerList = new EventListenerList();
	  protected WebParser wp;
	  protected boolean condition = true;
	  protected String increase;
	  protected String current;
	  
	  public ServerMonitor (){
		  wp = new WebParser();
		  this.start();
	  }
	  /**
	   * Description: this method was implement from thread and is used to check if there are any updates of data.
	   */
	  public void run(){
		while(condition){
			try{
			Thread.sleep(1000);
			}catch(InterruptedException e){
				//Handle Exception
			}
			try{
			wp.Update();
			System.out.println("Updating");
			String nextCurrent  = wp.getCurrent();
			String nextIncrease = wp.getIncrease();
			dataChange(nextCurrent, nextIncrease);
			}catch(Exception e){
				//Handle Exception
			}

		}
	  }
/**
 * Description: This method compares two new received values with older values.
 * @param nCurrent
 * @param nIncrease
 */
	  private void dataChange (String nCurrent, String nIncrease){
		  if(!(nCurrent == "") && !(nIncrease == "")){
			  if (!(nCurrent.equals(current)) || !(nIncrease.equals(increase))){
				  System.out.println("values changed");
				  current = nCurrent;
				  increase = nIncrease;
				  this.fireMyEvent(new MyEvent(this));
			  }
		  }
	  }
	  /**
	   * Description:Stops the looping in the thread.
	   */
	  public void stopMonitoring (){
		  condition = false;
	  }
	  /**
	   * Description:Starts the looping in the thread.
	   */
	  public void startMonitoring (){
		  condition = true;
	  }
	  /**
	   * Description: Returns increase value.
	   * @return increase
	   */
	  public String getIncrease (){
		  return increase;
	  }
	  /**
	   * Description: Returns current value.
	   * @return current
	   */
	  public String getCurrent (){
		  return current;
	  }
	  /**
	   * Description: Adds listener
	   * @param listener
	   */
	  public void addMyEventListener(DataChangeEvent listener) {
	    listenerList.add(DataChangeEvent.class, listener);
	  }
	  /**
	   * Description: Remove listener
	   * @param listener
	   */
	  public void removeMyEventListener(DataChangeEvent listener) {
	    listenerList.remove(DataChangeEvent.class, listener);
	  }
	  /**
	   * Description: fires the even on all applied listeners
	   * @param evt
	   */
	  void fireMyEvent(MyEvent evt) {
	    Object[] listeners = listenerList.getListenerList();
	    for (int i = 0; i < listeners.length; i = i+2) {
	      if (listeners[i] == DataChangeEvent.class) {
	        ((DataChangeEvent) listeners[i+1]).DataChangedOccurred(evt);
	      }
	    }
	  }
	  
}
/**
 * Description: Event object
 * @author Josh
 *
 */
class MyEvent extends EventObject {
	  public MyEvent(Object source) {
	    super(source);
	  }
}